"use strict";
var canvas, ctx, h, w, n, x = [], y = [], coords = [];
var btn = document.querySelector('.btn');
window.onload = function () {
    canvas = document.getElementById("tutorial");
    ctx = canvas.getContext("2d");
    h = canvas.height;
    w = canvas.width;
    alert("Enter coords like this: 23, 1, 4, 158, 100, 1, 37, 44");
};
function Click() {
    var str = document.getElementById("txt").value;
    coords = str.split(", ");
    n = coords.length;
    x = [];
    y = [];
    ctx.strokeStyle = "black";
    ctx.clearRect(0, 0, w, h);
    ctx.beginPath();
    ctx.moveTo(0, h / 2);
    ctx.lineTo(w, h / 2);
    ctx.moveTo(w / 2, 0);
    ctx.lineTo(w / 2, h);
    ctx.closePath();
    ctx.stroke();
    ctx.strokeStyle = "red";
    for (var j = 0; j < n; j += 2) {
        x[j / 2] = Number(coords[j]);
    }
    for (var j = 1; j < n; j += 2) {
        y[Math.trunc(j / 2)] = Number(coords[j]);
    }
    var square = 0;
    n = Math.trunc(n / 2);
    for (var i = 0; i < n - 1; i++) {
        ctx.beginPath();
        ctx.moveTo(x[i] + w / 2, h - (y[i] + h / 2));
        ctx.lineTo(x[i + 1] + w / 2, h - (y[i + 1] + h / 2));
        ctx.closePath();
        ctx.stroke();
        square += x[i] * y[i + 1] - y[i] * x[i + 1];
    }
    ctx.beginPath();
    ctx.moveTo(x[n - 1] + w / 2, h - (y[n - 1] + h / 2));
    ctx.lineTo(x[0] + w / 2, h - (y[0] + h / 2));
    ctx.closePath();
    ctx.stroke();
    square += x[n - 1] * y[0] - y[n - 1] * x[0];
    square /= 2;
    square = Math.abs(square);
    alert("Area = " + square + ".");
}
btn.addEventListener('click', Click);
